package main

import (
	"archive/tar"
	"bufio"
	"io"
	"log"
	"os"
	"path"

	"github.com/klauspost/readahead"
	flag "github.com/spf13/pflag"
)

type processParams struct {
	prefix      string
	concatenate bool
	cleanPath   bool
}

func main() {
	var verbose int
	var params processParams
	flag.CountVarP(&verbose, "verbose", "v", "add verbosity")
	flag.StringVarP(&params.prefix, "prefix", "p", "", "adds prefix to filenames")
	flag.BoolVarP(&params.concatenate, "concatenate", "c", false, "concatenate tar archives")
	flag.BoolVarP(&params.cleanPath, "clean-path", "", false, "clean tar entry path")
	flag.Parse()

	logger := NewLogger(LevelEnum(verbose) + 2)
	if err := process(params, logger); err != nil {
		log.Fatal(err)
	}
}

func process(params processParams, logger *Logger) error {
	in := readahead.NewReader(os.Stdin)
	defer in.Close()
	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	tr := tar.NewReader(in)
	tw := tar.NewWriter(out)
	defer tw.Close()
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			if params.concatenate {
				tr = tar.NewReader(in)
				hdr, err = tr.Next()
				if err == io.EOF {
					break
				} else if err != nil {
					return err
				}
			} else {
				break
			}
		} else if err != nil {
			return err
		}

		logger.Infof(`TarEntry.name = %s`, hdr.Name)
		if params.cleanPath {
			hdr.Name = path.Clean("/" + hdr.Name)[1:]
		}
		hdr.Name = params.prefix + hdr.Name

		if err = tw.WriteHeader(hdr); err != nil {
			return err
		}
		if _, err := io.Copy(tw, tr); err != nil {
			return err
		}
	}
	if err := tw.Flush(); err != nil {
		return err
	}

	return nil
}
