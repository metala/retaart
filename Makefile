rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

.PHONY: all

bin/retaart: Makefile $(call rwildcard,.,*.go)
	CGO_ENABLED=0 go build -ldflags="-extldflags=-static" -o bin/retaart .

install: bin/retaart
	cp bin/retaart ~/bin/
